#this project works with willump>=1.2.0
import willump

import asyncio
import logging

import subprocess
import sys
import xlsxwriter

list_perso = []
list_skin = []
list_get = []

def install_dependency(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])


async def get_summoner_data():
    summoner = await (await wllp.request("GET", "/lol-summoner/v1/current-summoner")).json()
    print(f"summonerName:    {summoner['displayName']}")
    print(f"summonerLevel:   {summoner['summonerLevel']}")
    print(f"profileIconId:   {summoner['profileIconId']}")
    print(f"summonerId:      {summoner['summonerId']}")
    print(f"puuid:           {summoner['puuid']}")
    print(f"---")
    return summoner['summonerId']


async def get_summoner_loot_skins_pinkable():
    skins = await (await wllp.request('get', '/lol-loot/v1/player-loot')).json()
    test  = []
    for skin in skins:
        if(skin["disenchantRecipeName"] == "SKIN_RENTAL_disenchant"):
            if(skin["itemDesc"] in list_skin):
                test.append(skin["itemDesc"])   
    return test


async def get_challenges():
    test = await (await wllp.request('get', '/lol-challenges/v1/level-points')).json() 
    return test

async def get_list_skins():
    ID = await get_summoner_data()
    reported = await (await wllp.request('get', '/lol-champions/v1/inventories/'+str(ID)+'/champions')).json()
    for perso in reported:
        for skin in perso["skins"]:
            for chroma in skin["chromas"]:
                if("#E58BA5" in chroma["colors"] or "#FF95EC" in chroma["colors"]):
                    list_perso.append(perso["name"])
                    list_skin.append(skin["name"])
                    
    crafts = await get_summoner_loot_skins_pinkable()
    for perso in reported:
        for  skin in perso["skins"]:
            logging.info(str(skin))
            for chroma in skin["chromas"]:
                if("#E58BA5" in chroma["colors"] or "#FF95EC" in chroma["colors"]):

                    if(skin["ownership"]["owned"]):
                        list_get.append("✅")
                    else:
                        if(skin["name"] in crafts):
                            list_get.append("⏳")
                        else:
                            list_get.append("")


dependencies = ['willump','numpy','xlsxwriter']
# Installation des dépendances
for dependency in dependencies:
    try:
        print(dependency)
        import_module = __import__(dependency)
    except ImportError:
        print(f'{dependency} n\'est pas installé. Installation en cours...')
        install_dependency(dependency)

async def main():
    logging.basicConfig(filename='example.log', encoding='utf-8', level=logging.DEBUG)
    global wllp
    import numpy
    wllp = await willump.start()
    await get_list_skins()

    workbook = xlsxwriter.Workbook('chroma.xlsx')
    worksheet = workbook.add_worksheet()
    resultat = numpy.column_stack((list_perso, list_skin, list_get))

    # Start from the first cell. Rows and columns are zero indexed.
    row = 0
    col = 0

    # Iterate over the data and write it out row by row.
    for perso, skin, value in (resultat):
        worksheet.write(row, col,     perso)
        worksheet.write(row, col + 1, skin)
        worksheet.write(row, col + 2, value)
        row += 1
        
    workbook.close()


if __name__ == '__main__':
    # uncomment this line if you want to see nunu complain (debug log)
    logging.getLogger().setLevel(level=logging.DEBUG)
    try:
        asyncio.get_event_loop().run_until_complete(main())
    except KeyboardInterrupt:
        asyncio.run(wllp.close())